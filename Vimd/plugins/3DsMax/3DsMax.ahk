﻿3DsMax:
;20181114   修正输入状态延迟
;20181114   修正activeType本地初始化
;20181102   修复载入渲染乱码显示！
;20181102   修正渲染附加组件无法使用，加入预加载列表.

;20181023   对接口路径做判断修复只能打开一次，主要涉及到脚本本身
;20181022   新增加对MaxScript的支持,采用第3方接口MXSPyCOM.exe,配置
;           %A_ScriptDir%\custom\maxScripts\initialize_COM_server.ms 拷贝到...3ds Max 2014\scripts\Startup
;20180920   新增输入状态屏蔽功能
;20180919   新增activeType设置快捷为Ctrl+Shift+Alt+C
;;Windows Groups

;提前运行

; GroupAdd, 3dsMax, ahk_class 3DSMAX
; GroupAdd, MatEditor,Material Editor
; GroupAdd, 3dsMax, ahk_class AutoHotkeyGUI

;待解决
;1.Octopus本地化
;2 http://www.scriptspot.com/3ds-max/scripts/dropbox看使用率在集成
;3.IKMAX本地化


;;3ds Max
;#IfWinActive ahk_group 3dsMax

;定义注释
    vim.SetAction("<3DsMax_NormalMode>", "返回正常模式")
    vim.SetAction("<3DsMax_InsertMode>", "进入VIM模式")
    vim.SetWin("3DSMAX","ahk_exe","3dsmax.exe")
;normal模式（必需）
    vim.SetMode("normal", "3DSMAX")
    vim.map("<insert>","<3DsMax_InsertMode>","3DSMAX")

    vim.Comment("<max_render8Direction>", "八方向渲染/批渲染")
    vim.Comment("<max_saveAniTime>", "动作记录")
    vim.Comment("<max_maxToTotalcmd>","3dsmaxToTotalcmd")

    vim.Comment("<ShowHelp>","帮助")
    vim.Comment("<Max_Save>","保存")
    vim.Comment("<Max_SaveAs>","保存")
    vim.Comment("<Max_Open>","打开")
    vim.Comment("<Max_Exit>","退出")
    vim.Comment("<Max_Archive>","打包")
    vim.Comment("<Max_Reset>","重置")
    vim.Comment("<Max_Import>","导入")
    vim.Comment("<Max_Export>","导出")
    vim.Comment("<Max_UpDater>","更新")

;insert模式
    vim.SetMode("insert", "3DsMax")
    vim.Map("<esc>", "<3DsMax_NormalMode>", "3DsMax")
    vim.Map("q", "<Max_Double_Q>", "3DSMAX")
    ;vim.Map("qq", "<Max_Double_Q>", "3DSMAX")
    vim.Map("w", "<Max_Double_W>", "3DSMAX")
    vim.Map("z", "<Max_Double_Z>", "3DSMAX")
    vim.Map("y", "<Max_Double_Y>", "3DSMAX")
    vim.Map("o", "<Max_Double_O>", "3DSMAX")
    vim.Map("x", "<Max_Double_X>", "3DSMAX")
    vim.Map("XX", "<max_maxPos>", "3DSMAX")
    vim.Map("Xa", "<max_maxXYZ>", "3DSMAX")
    vim.Map("s", "<Max_Double_S>", "3DSMAX")
    vim.Map("0", "<Max_Numpad0>", "3DSMAX")
    vim.Map("1", "<Max_activeType>", "3DSMAX")   
    vim.Map("R", "<max_maxRotate90>", "3DSMAX")  
    vim.map("?","<ShowHelp>","3DSMAX")



;<Numpad1>

    vim.map("/1","<Max_Open>","3DSMAX")
    vim.map("/2","<Max_Reset>","3DSMAX")
    vim.map("/3","<Max_Save>","3DSMAX")
    vim.map("/4","<Max_SaveAs>","3DSMAX")
    vim.map("/5","<Max_Archive>","3DSMAX")
    vim.map("/6","<Max_Exit>","3DSMAX")
    vim.map("/7","<Max_Import>","3DSMAX")
    vim.map("/8","<Max_Export>","3DSMAX")
    vim.map("/9","<Max_UpDater>","3DSMAX")
    
    vim.map("<F7>","<max_maxToTotalcmd>","3DSMAX")
    vim.Map("<F8>1", "<max_render8Direction>", "3DSMAX")
    vim.Map("<F9>", "<Max_Double_F9>", "3DSMAX")
    vim.BeforeActionDo("3DsMax_CheckMode", "3DSMAX")


return
;检测
3DsMax_CheckMode(){
    ControlGetFocus, ctrl, A
    ; msgbox, ctrl ||RegExMatch(ctrl, "i)ComboBox")||RegExMatch(ctrl, "i)Edit1")
    ;if RegExMatch(ctrl, "i)Edit")||RegExMatch(ctrl, "i)MXS_Scintilla")||RegExMatch(ctrl, "i)EDITDUMMY")||RegExMatch(ctrl, "i)MXS_Scintilla1")||RegExMatch(ctrl, "i)MXS_Scintilla2")||RegExMatch(ctrl, "i)WindowsForms10.EDIT.app.0.34f5582_r46_ad1")||RegExMatch(ctrl, "i)ComboBox") ;  or WinExist("ahk_class MXS_SciTEWindow")) ;|| RegExMatch(ctrl, "i)MXS_Scintilla1")
    if RegExMatch(ctrl, "i)Edit")||RegExMatch(ctrl, "i)MXS_Scintilla")||RegExMatch(ctrl, "i)EDITDUMMY")||RegExMatch(ctrl, "i)WindowsForms10")||RegExMatch(ctrl, "i)ComboBox") ;  or WinExist("ahk_class MXS_SciTEWindow")) ;|| RegExMatch(ctrl, "i)MXS_Scintilla1")
    return true
    return false
}

;预加载
3DsMax_StartupRun(){
    run, %A_ScriptDir%\custom\maxScripts\MXSPyCOM.exe -f %A_ScriptDir%\custom\maxScripts\lib\setEvn.ms  ; 设置脚本新目录
    run, %A_ScriptDir%\custom\maxScripts\MXSPyCOM.exe -f %A_ScriptDir%\custom\maxScripts\Startup\R8D_WriteICON.ms
    run, %A_ScriptDir%\custom\maxScripts\MXSPyCOM.exe -f %A_ScriptDir%\custom\maxScripts\Startup\r8d4.ms
return
}

;【全局运行Max】
<RunMax>:
    MaxPath := ini.BOBOPath_Config.MaxPath
    Max_Class := ini.ahk_class_Config.Max_Class
    DetectHiddenWindows, on
    IfWinNotExist MaxPath Max_Class 
    { 
   		Run %MaxPath%
    	WinActivate 
    } 
    	Else IfWinNotActive ahk_class Max_Class 
    { 
    	WinActivate
    } 
    Else 
    { 
        WinMinimize 
    } 
 Return

<3DsMax_NormalMode>:
;   send,{esc}
    vim.SetMode("normal", "3DsMax")
    MsgBox, 0, 提示, 【正常模式】, 0.5
return

<3DsMax_InsertMode>:
;   send,{esc}
    vim.SetMode("insert", "3DsMax")
    MsgBox, 0, 提示, 【VIM模式】, 0.5
return

;基本操作
<Max_Save>:
{
    Send,^s
}
return
<Max_Open>:
{
    Send,^o
}
return
<Max_SaveAs>:
{
    run, %A_ScriptDir%\custom\maxScripts\MXSPyCOM.exe -f %A_ScriptDir%\custom\maxScripts\commands\SaveAs.ms 
}
return
<Max_Exit>:
{
    run, %A_ScriptDir%\custom\maxScripts\MXSPyCOM.exe -f %A_ScriptDir%\custom\maxScripts\commands\Exit.ms 
}
return
<Max_Archive>:
{
    run, %A_ScriptDir%\custom\maxScripts\MXSPyCOM.exe -f %A_ScriptDir%\custom\maxScripts\commands\Archive.ms 

}
return
<Max_Reset>:
{
    run, %A_ScriptDir%\custom\maxScripts\MXSPyCOM.exe -f %A_ScriptDir%\custom\maxScripts\commands\Reset.ms 
}
return

<Max_Import>:
{
    run, %A_ScriptDir%\custom\maxScripts\MXSPyCOM.exe -f %A_ScriptDir%\custom\maxScripts\commands\Import.ms 
}
return
<Max_Export>:
{
    run, %A_ScriptDir%\custom\maxScripts\MXSPyCOM.exe -f %A_ScriptDir%\custom\maxScripts\commands\Export.ms 
}
return

;end

;双按操作
<Max_Double_Z>:
{
    t := A_PriorHotkey == A_ThisHotkey && A_TimeSincePriorHotkey < 200 ? "off" : -200
    settimer, max_tappedkey_z, %t%
    if (t == "off")
    goto max_double_z
    return

    max_tappedkey_z:
    Send z
    return

    max_double_z:
    Send, ^+z
    return
}
 Return


<Max_Double_O>:
{
    t := A_PriorHotkey == A_ThisHotkey && A_TimeSincePriorHotkey < 200 ? "off" : -200
    settimer, Max_tappedkey_o, %t%

    if (t == "off")
    goto Max_double_o
    return

    Max_tappedkey_o:
    Send, o
    return

    Max_double_o:
    Send,^o
    return  
}
Return

<Max_Double_Q>:
{
    t := A_PriorHotkey == A_ThisHotkey && A_TimeSincePriorHotkey < 200 ? "off" : -200
    settimer, Max_tappedkey_q, %t%

    if (t == "off")
    goto Max_double_q
    return

    Max_tappedkey_q:
    Send, q
    return

    Max_double_q:
    Send,+q
    return  
}
Return

<Max_Double_W>:
{
    t := A_PriorHotkey == A_ThisHotkey && A_TimeSincePriorHotkey < 200 ? "off" : -200
    settimer, Max_tappedkey_w, %t%

    if (t == "off")
    goto Max_double_w
    return

    Max_tappedkey_w:
    Send, w
    return

    Max_double_w:
    Send,!w
    return  
}
Return


<Max_Double_X>:
{
    t := A_PriorHotkey == A_ThisHotkey && A_TimeSincePriorHotkey < 200 ? "off" : -200
    settimer, max_tappedkey_x, %t%
    if (t == "off")
    goto max_double_x
    return

    max_tappedkey_x:
    Send x
    return

    max_double_x:
    Send, ^!+{x}
    return
}
Return


<Max_Double_Y>:
{
    t := A_PriorHotkey == A_ThisHotkey && A_TimeSincePriorHotkey < 200 ? "off" : -200
    settimer, max_tappedkey_y, %t%
    if (t == "off")
    goto max_double_y
    return

    max_tappedkey_y:
    Send {y}
    return

    max_double_y:
    Send, ^!+{y}
    return
}
Return


<Max_Double_S>:
{
    t := A_PriorHotkey == A_ThisHotkey && A_TimeSincePriorHotkey < 200 ? "off" : -200
    settimer, max_tappedkey_s, %t%
    if (t == "off")
    goto max_double_s
    return

    max_tappedkey_s:
    Send {s}
    return

    max_double_s:
    Send, ^{s}
    return
}
Return

<Max_Double_F9>:
{
    t := A_PriorHotkey == A_ThisHotkey && A_TimeSincePriorHotkey < 200 ? "off" : -200
    settimer, max_tappedkey_F9, %t%
    if (t == "off")
    goto max_double_F9
    return

    max_tappedkey_F9:
    Send {F9}
    return

    max_double_F9:
    Send, {F10}
    return
}
Return



;播放暂停
<Max_Numpad0>:
{
    Send, /
}
Return

; Scrolllock::
; Suspend,Toggle
; Return

; +Scrolllock::
; reloadgg
; return

<Max_activeType>:
{
    run, %A_ScriptDir%\custom\maxScripts\MXSPyCOM.exe -f %A_ScriptDir%\custom\maxScripts\activeType.ms 
}
Return

;动作保存模块
; <max_saveAniTime>:
; {
;     run, %A_ScriptDir%\custom\maxScripts\MXSPyCOM.exe -f %A_ScriptDir%\custom\maxScripts\saveAniTime.ms 
;     return
; }

;批渲染脚本
<max_render8Direction>:
{
    run, %A_ScriptDir%\custom\maxScripts\MXSPyCOM.exe -f %A_ScriptDir%\custom\maxScripts\Render8Direction.ms
    return
}
;
<max_maxToTotalcmd>:
{
    run, %A_ScriptDir%\custom\maxScripts\MXSPyCOM.exe -f %A_ScriptDir%\custom\maxScripts\maxToTotalcmd.ms
    return
}


;功能性脚本

<max_maxRotate90>:
{
    test = rotate $ (angleaxis 90 [0,0,1])
    ControlFocus, MXS_Scintilla2, ahk_exe 3dsmax.exe
    ControlSetText, MXS_Scintilla2, %test%, ahk_exe 3dsmax.exe
    send, +{Enter}
    Return
}



; 只在3dsmax界面上生效的热键，这个就只是在max的Label1窗口起作用，
; 在其他地方6还是6，
; #If ActiveControlIs("Label1")
; 6::Send {F5}
; 7::MsgBox
; #If

; ActiveControlIs(Control) {
;     ControlGetFocus, FocusedControl, A
;     return (FocusedControl=Control)
; }

;位置坐标操作
;物体归00点
<max_maxPos>:
{
    run, %A_ScriptDir%\custom\maxScripts\MXSPyCOM.exe -f %A_ScriptDir%\custom\maxScripts\commands\XYZ_0.ms
    return
}
;坐标归物体00点
<max_maxXYZ>:
{
    run, %A_ScriptDir%\custom\maxScripts\MXSPyCOM.exe -f %A_ScriptDir%\custom\maxScripts\commands\XYZ_Z.ms
    return 
}
;更新
<Max_UpDater>:
Gui,Updating: +LastFound +AlwaysOnTop -Caption +ToolWindow
Gui,Updating: Color, %color2%
Gui,Updating: Font,cwhite s%FontSize% wbold q5,Segoe UI
Gui,Updating: Add, Text, ,%_MaxUpdating%
Gui,Updating: Show,AutoSize Center NoActivate
UrlDownloadToFile, %UrlDownloadToFile_Ae1%, %A_ScriptDir%\plugins\3DsMax\latest-3DsMax.ahk ;

if ErrorLevel
{
    Gosub, MaxExitUpdater
}
Else
{
    FileMove,%A_ScriptDir%\plugins\3DsMax\latest-3DsMax.ahk, %A_ScriptDir%\plugins\3DsMax\3DsMax.ahk,1
    Gosub, MaxExitUpdater
}
return

MaxExitUpdater:
FileDelete, %A_ScriptDir%\plugins\3DsMax\latest-3DsMax.ahk
sleep %SleepTime%
Gui,Updating: Hide
Gui,Updating2: +LastFound +AlwaysOnTop -Caption +ToolWindow
Gui,Updating2: Color, %color3%
Gui,Updating2: Font,cwhite s%FontSize% wbold q5,Segoe UI
Gui,Updating2: Add, Text, ,%_UpdateCompleted%
Gui,Updating2: Show,AutoSize Center NoActivate
sleep %SleepTime%
Gui,Updating2: Hide
Run,%A_ScriptDir%\Vimd.exe
Return