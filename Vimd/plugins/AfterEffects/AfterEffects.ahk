﻿;Author:BoBO
;Version:2018-11-19
;
;####################\\\\\\使用本脚本请在双按zz下使用，效果最佳，否则会有点影响使用///////####################
;更新内容
;2018-12-04 新增双按8循环模式
;           新增自动遮罩分层
;2018-11-19 对图层按'dd'删除(帧/效果/表达式/遮罩)& 重置(Transfrom&All)
;2018-10-25 1.移植Ae脚本到VIMD，已经把常用功能移植到.
;           2.新增操作"/0-5" 打开|保存|最近打开|导入|退出|操作
;           3.优化使用频率 v系组合调整 vcc创建固态 vcn创建合成组
;2018-10-24 新增加zz窗口最大化功能，新增加此功能为了ahk调用脚本时窗口会缩放无法最大化严重影响体验，查阅Google目前还无法解决，但
;           好在窗口最大化环境下可以完美使用，下个版本将把脚本移动本地化运行，实现便携式.
;2018-09-20 新增加输入状态屏蔽功能
;
;功能备忘录
;
;1.一键刷新素材
;
;
;
AfterEffects:
    vim.SetAction("<AfterEffects_NormalMode>", "返回正常模式")
    vim.SetAction("<AfterEffects_InsertMode>", "进入VIM模式")
    vim.SetWin("AfterEffects","ahk_exe","AfterFX.exe")
;normal模式（必需）
    vim.SetMode("normal", "AfterEffects")
    vim.map("<insert>","<AfterEffects_InsertMode>","AfterEffects")
;定义注释
    vim.Comment("<Ae_Open>","打开")
    vim.Comment("<Ae_Save>","保存")
    vim.Comment("<Ae_SaveAs>","另存为")
    vim.Comment("<Ae_OpenRecent>","最近打开")
    vim.Comment("<Ae_Import>","导入")
    vim.Comment("<Ae_Exit>","退出")

    vim.Comment("<Ae_Script_BoboTools>","BoBO_AeTools")
    vim.Comment("<Ae_Script_QuickMenu>","QuickMenu")
    vim.Comment("<Ae_Script_KeyBoard>","KeyBoard")
    vim.Comment("<Ae_Script_CompSetter>","批合成重设")
    vim.Comment("<Ae_Script_batchFootage>","批素材重设")
    vim.Comment("<Ae_Script_AutoMatte>","AE自动遮罩分层")

    vim.Comment("<Ae_Double_q>","双按系_【q】_渲染")
    vim.Comment("<Ae_Double_c>","双按系_【c】_新建合成")
    vim.Comment("<Ae_Double_x>","双按系_【x】_退出AE")
    vim.Comment("<Ae_Double_z>","双按系_【z】_窗口100%%")
    vim.Comment("<Ae_Double_o>","双按系_【o】_打开")
    vim.Comment("<Ae_Double_y>", "层设置")
    vim.Comment("<Ae_Press_p>","长按系_K位置关键帧")
    vim.Comment("<Ae_Press_s>","长按系_K缩放关键帧")
    vim.Comment("<Ae_Press_r>","长按系_K旋转关键帧")
    vim.Comment("<Ae_Press_t>","长按系_K透明关键帧")
    vim.Comment("<Ae_OpenClose_Solo>","图层独立显示")
    vim.Comment("<Ae_OpenClose_Lock>","图层锁定")
    vim.Comment("<Ae_OpenClose_Hides>","图层隐藏显示")
    vim.Comment("<AfterEffects_新建合成>", "新建合成")
    vim.Comment("<AfterEffects_固态层>", "新建固态层")
    vim.Comment("<AfterEffects_调节层>", "新建调节层")
    vim.Comment("<AfterEffects_Null>", "新建Null")
    vim.Comment("<Ae_Remove>", "删除所有效果")
    vim.Comment("<ShowHelp2>","隐藏帮助")
    vim.Comment("<Ae_CllectFiles>","打包文件")
    vim.Comment("<Ae_AllMemoryDisk>","清理内存和数据")
    vim.Comment("<Ae_UpDater>","插件更新")
    vim.Comment("<Ae_Help>","帮助文档")
;预设动画说明
    vim.Comment("<Ae_Preset_Ani1>","说明是干嘛的")
;insert模式
    vim.SetMode("insert", "AfterEffects")
    vim.Map("<esc>", "<AfterEffects_NormalMode>", "AfterEffects")


    ;vim.Map("<F8>", "<AfterEffects_我的工具>", "AfterEffects")
    vim.Map("/0", "<Ae_Open>", "AfterEffects")
    vim.Map("/1", "<Ae_Save>", "AfterEffects")
    vim.Map("/2", "<Ae_SaveAs>", "AfterEffects")
    vim.Map("/3", "<Ae_OpenRecent>", "AfterEffects")
    vim.Map("/4", "<Ae_Import>", "AfterEffects")
    vim.Map("/5", "<Ae_Exit>", "AfterEffects")
    vim.Map("/6", "<Ae_CllectFiles>", "AfterEffects")
    vim.Map("/7", "<Ae_AllMemoryDisk>", "AfterEffects")
    vim.Map("/8", "<Ae_UpDater>", "AfterEffects")
    vim.Map("/9", "<Ae_Help>", "AfterEffects")
    

 ;  调用脚本
    vim.Map("<F1>", "<Ae_ui_y>", "AfterEffects")   
    vim.Map("<F2>", "<Ae_Script_KeyBoard>", "AfterEffects")                 ;调用KeyBoard
 ;  vim.Map("<F5>", "<AfterEffects_渲染输出>", "AfterEffects")
    vim.Map("<F6>", "<AfterEffects_优化合成时间>", "AfterEffects")            ;调用KeyBoard
;    vim.Map("<F7>", "<Ae_Script_QuickMenu>", "AfterEffects")
    vim.Map("<F8>1", "<Ae_Script_BoboTools>", "AfterEffects")  
    vim.Map("<F8>2", "<Ae_Script_CompSetter>", "AfterEffects") 
    vim.Map("<F8>3", "<Ae_Script_batchFootage>", "AfterEffects")
    vim.Map("<F8>4", "<Ae_Script_AutoMatte>", "AfterEffects")
    vim.Map("<F9>", "<Ae_Double_F9>", "AfterEffects")  
    vim.Map("<F12>", "<Ae_Script_AEProject>", "AfterEffects")


    vim.map("?","<ShowHelp>","AfterEffects")
    vim.map(":?","<ShowHelp2>","AfterEffects")
;Double类型
    vim.Map("x", "<Ae_Double_x>", "AfterEffects")
    vim.Map("q", "<Ae_Double_q>", "AfterEffects")
    vim.Map("c", "<Ae_Double_c>", "AfterEffects")
    vim.Map("z", "<Ae_Double_z>", "AfterEffects")
    vim.Map("o", "<Ae_Double_o>", "AfterEffects")
    vim.Map("k", "<Ae_Double_k>", "AfterEffects")
    vim.Map("y", "<Ae_Double_y>", "AfterEffects")
    vim.Map("d", "<Ae_Double_d>", "AfterEffects")
    vim.Map("8", "<Ae_Double_8>", "AfterEffects")
;Press长按类型
    vim.Map("p", "<Ae_Press_p>", "AfterEffects")
    vim.Map("s", "<Ae_Press_s>", "AfterEffects")
    vim.Map("r", "<Ae_Press_r>", "AfterEffects")
    vim.Map("t", "<Ae_Press_t>", "AfterEffects")
;RunScript
    vim.Map("X", "<Ae_OpenClose_Solo>", "AfterEffects")
    vim.Map("C", "<Ae_OpenClose_Lock>", "AfterEffects")
    vim.Map("Z", "<Ae_OpenClose_Hides>", "AfterEffects")
;调用预设按键
    vim.Map("A1", "<Ae_Preset_Ani1>", "AfterEffects")
    vim.Map("A2", "<Ae_Preset_Ani2>", "AfterEffects")
    vim.Map("A3", "<Ae_Preset_Ani3>", "AfterEffects")
    vim.Map("A4", "<Ae_Preset_Ani4>", "AfterEffects")
    vim.Map("A5", "<Ae_Preset_Ani5>", "AfterEffects")

    vim.Map("R", "<Ae_Remove>", "AfterEffects")

    vim.Map("vcn", "<AfterEffects_新建合成>", "AfterEffects")
    vim.Map("vcc", "<AfterEffects_固态层>", "AfterEffects")
    vim.Map("vca", "<AfterEffects_调节层>", "AfterEffects")
    vim.Map("vcm", "<AfterEffects_Null>", "AfterEffects")

    vim.BeforeActionDo("AE_CheckMode", "AfterEffects") 
return

;判断输入状态屏蔽
AE_CheckMode(){
    ControlGetFocus, ctrl, A
    ; msgbox, ctrl
    if RegExMatch(ctrl,"i)Edit") ; or WinExist("ahk_class VCSDK_WINDOW_CLASS")) ;增加FXConsole
        return true
    return false
}

<AE_热键帮助>:  ;{
;    QZ_VIMD_ShowKeyHelp()
return
;}


;【全局运行AE】
<RunAE>:
    AEPath := ini.BOBOPath_Config.AEPath
    AE_Class := ini.ahk_class_Config.AE_Class
    DetectHiddenWindows, on
    IfWinNotExist AePath AE_Class 
    { 
   		Run %AEPath% ;是否增加多开模式 不想要去掉
    	WinActivate 
    } 
    	Else IfWinNotActive ahk_class AE_Class 
    { 
    	WinActivate
    } 
    Else 
    { 
        WinMinimize 
    } 

 Return

<AfterEffects_NormalMode>:
;   send,{esc}
    vim.SetMode("normal", "AfterEffects")
    MsgBox, 0, 提示, 【正常模式】, 0.5
return

<AfterEffects_InsertMode>:
;   send,{esc}
    vim.SetMode("insert", "AfterEffects")
    MsgBox, 0, 提示, 【VIM模式s】, 0.5
return

<Ae_Remove>:
    send, ^+{e}
return

;基本
<Ae_Open>:
{
    send,^o
    return
}
<Ae_OpenRecent>:
{
    send,^!+p
    return
}
<Ae_Save>:
{
    send,^s
    return
}
<Ae_SaveAs>:
{
    send,^+s
    return
}
<Ae_Import>:
{
    send,^i
    return
}
<Ae_Exit>:
{
    send,^q
    return
}

<Ae_CllectFiles>:
{
    WinGet, activePath, ProcessPath, % "ahk_id" winActive("A")
    tool_pathandname = "%activePath%"

    run, %comspec% /c %tool_pathandname% -s  app.executeCommand(2482); ,,Hide

    return
}

<Ae_AllMemoryDisk>:
{
    WinGet, activePath, ProcessPath, % "ahk_id" winActive("A")
    tool_pathandname = "%activePath%"

    run, %comspec% /c %tool_pathandname% -s  app.executeCommand(10200); ,,Hide

    return
}


;新建
<AfterEffects_新建合成>:
    send, ^n
return

<AfterEffects_固态层>:
    send, ^y
return

<AfterEffects_调节层>:
    send, ^!y
return

<AfterEffects_Null>:
    send, ^!+y
return

<AfterEffects_优化合成时间>:
    send, ^+x
Return

<AfterEffects_渲染输出>:
    send, ^m
Return

<AfterEffects_QuickMenu>:
    send, ^!6
return

<AfterEffects_我的工具>:
    send, ^!7
return

;面板

<Ae_ui_y>:
{
    return
}
return
; Double组


; Double_K 合成设置
; #if WinActive("ahk_class Qt5QWindowIcon")
<Ae_Double_8>:
{
    t := A_PriorHotkey == A_ThisHotkey && A_TimeSincePriorHotkey < 200 ? "off" : -200
    settimer, ae_tappedkey_8, %t%
    if (t == "off")
    goto ae_double_8
    return

    ae_tappedkey_8:
    Send, 8
    return

    ae_double_8:
        ; 循环工具
        WinGet, activePath, ProcessPath, % "ahk_id" winActive("A")
        tool_pathandname = "%activePath%"
        run, %comspec% /c %tool_pathandname% -s -ro %A_ScriptDir%\custom\ae_scripts\(BOBOToolsFunctions)\LoopMaker.jsxbin ,,Hide

    return
}
<Ae_Double_F9>:
{
    t := A_PriorHotkey == A_ThisHotkey && A_TimeSincePriorHotkey < 200 ? "off" : -200
    settimer, ae_tappedkey_F9, %t%
    if (t == "off")
    goto ae_double_F9
    return

    ae_tappedkey_F9:
    Send, ^{m}
    return

    ae_double_F9:

        WinGet, activePath, ProcessPath, % "ahk_id" winActive("A")
        tool_pathandname = "%activePath%"
        run, %comspec% /c %tool_pathandname% -s -ro %A_ScriptDir%\custom\ae_scripts\(BOBOToolsFunctions)\SPRenderQueueTools.jsx ,,Hide

    return
}
Return

<Ae_Double_y>:
{
    t := A_PriorHotkey == A_ThisHotkey && A_TimeSincePriorHotkey < 200 ? "off" : -200
    settimer, ae_tappedkey_y, %t%
    if (t == "off")
    goto ae_double_y
    return

    ae_tappedkey_y:
    Send, ^+{y}
    return

    ae_double_y:
    send, y
    return
}
Return

<Ae_Double_d>:
{
    t := A_PriorHotkey == A_ThisHotkey && A_TimeSincePriorHotkey < 200 ? "off" : -200
    settimer, ae_tappedkey_d, %t%
    if (t == "off")
    goto ae_double_d
    return

    ae_tappedkey_d:
    Send, d
    return

    ae_double_d:
    WinGet, activePath, ProcessPath, % "ahk_id" winActive("A")
    tool_pathandname = "%activePath%"
    run, %comspec% /c %tool_pathandname% -ro %A_ScriptDir%\\custom\\ae_scripts\\commands\QuickDeleteAndReset.jsxbin ,,Hide
    return
}
Return

<Ae_Double_k>:
{
    t := A_PriorHotkey == A_ThisHotkey && A_TimeSincePriorHotkey < 200 ? "off" : -200
    settimer, ae_tappedkey_k, %t%
    if (t == "off")
    goto ae_double_k
    return

    ae_tappedkey_k:
    Send, k
    return

    ae_double_k:
    send, ^k
    return
}
Return

; Double_Q 渲染输出
; #if WinActive("ahk_class Qt5QWindowIcon")
<Ae_Double_q>:
{
    t := A_PriorHotkey == A_ThisHotkey && A_TimeSincePriorHotkey < 200 ? "off" : -200
    settimer, ae_tappedkey_q, %t%
    if (t == "off")
    goto ae_double_q
    return

    ae_tappedkey_q:
    Send {q}
    return

    ae_double_q:
    send, ^m
    return
}
Return

; Double_C 合成组
<Ae_Double_c>:
{
    t := A_PriorHotkey == A_ThisHotkey && A_TimeSincePriorHotkey < 200 ? "off" : -200
    settimer, ae_tappedkey_c, %t%
    if (t == "off")
    goto ae_double_c
    return

    ae_tappedkey_c:
    Send c
    return

    ae_double_c:
    send, ^+c
    return
}
Return

; Double_C 合成组
<Ae_Double_x>:
{
    t := A_PriorHotkey == A_ThisHotkey && A_TimeSincePriorHotkey < 200 ? "off" : -200
    settimer, ae_tappedkey_x, %t%
    if (t == "off")
    goto ae_double_x
    return 

    ae_tappedkey_x:
    Send ^w
    return 

    ae_double_x:
    send, ^q
    return
}
Return

; Double_z 最大化显示
<Ae_Double_z>:
{
    t := A_PriorHotkey == A_ThisHotkey && A_TimeSincePriorHotkey < 200 ? "off" : -200
    settimer, ae_tappedkey_z, %t%
    if (t == "off")
    goto ae_double_z
    return

    ae_tappedkey_z:
    Send, /
    return

    ae_double_z:
    send, ^!\
    return
}
Return

; Double_o 最大化显示
<Ae_Double_o>:
{
    t := A_PriorHotkey == A_ThisHotkey && A_TimeSincePriorHotkey < 200 ? "off" : -200
    settimer, ae_tappedkey_o, %t%

    if (t == "off")
    goto ae_double_o
    return

    ae_tappedkey_o:
    Send, o
    return

    ae_double_o:
    Send,^o
    return  
}
Return



; 长按

;長鍵add position key
<Ae_Press_p>:
{
    KeyWait,p
    if A_TimeSinceThisHotkey >= 300 ; in milliseconds.
    {
        send !+p
    }
    else
        send p
    return
}

;长按add scale key
<Ae_Press_s>:
{
    KeyWait,s
    if  A_TimeSinceThisHotkey >= 300  ; in milliseconds.
    {
        send !+s
    }
    else
        send s
    return
}

;长按add rotation key0
<Ae_Press_r>:
{
    KeyWait,r
    if A_TimeSinceThisHotkey >= 300 ; in milliseconds.
    {
        send, !+r
    }
    else
        send, r
    return
}

;长按add opacity key
<Ae_Press_t>:
{
    KeyWait,t
    if A_TimeSinceThisHotkey >= 300 ; in milliseconds.
    {
        send, !+t
    }
    else
        send, t
    return
}

;层_独立显示 
;
<Ae_OpenClose_Solo>:
{
    WinGet, activePath, ProcessPath, % "ahk_id" winActive("A")
    tool_pathandname = "%activePath%"

    run, %comspec% /c %tool_pathandname% -s  app.executeCommand(2566); ,,Hide

    return
}
;层_关闭眼睛 
<Ae_OpenClose_Hides>:
{
    WinGet, activePath, ProcessPath, % "ahk_id" winActive("A")
    tool_pathandname = "%activePath%"

    run, %comspec% /c %tool_pathandname% -s  app.executeCommand(2059); ,,Hide

    return

}
;层_锁定 
<Ae_OpenClose_Lock>:
{
    WinGet, activePath, ProcessPath, % "ahk_id" winActive("A")
    tool_pathandname = "%activePath%"

    run, %comspec% /c %tool_pathandname% -s  app.executeCommand(2114); ,,Hide

    return

}

<ShowHelp2>:
{
    WinGet, activePath, ProcessPath, % "ahk_id" winActive("A")
    tool_pathandname = "%activePath%"

    run, %comspec% /c %tool_pathandname% -ro %A_ScriptDir%\\custom\\ae_scripts\ShowHelp2.jsx ,,Hide

    return
}

;调用Script
<Ae_Script_Render>:
{
    WinGet, activePath, ProcessPath, % "ahk_id" winActive("A")
    tool_pathandname = "%activePath%"
    run, %comspec% /c %tool_pathandname% -s -ro %A_ScriptDir%\custom\ae_scripts\(BOBOToolsFunctions)\SPRenderQueueTools.jsx ,,Hide
    return  
}

<Ae_Script_BoboTools>:
{
    WinGet, activePath, ProcessPath, % "ahk_id" winActive("A")
    tool_pathandname = "%activePath%"
    run, %comspec% /c %tool_pathandname% -s -ro %A_ScriptDir%\custom\ae_scripts\02_BOBO_Tools.jsx ,,Hide
    return
}

<Ae_Script_KeyBoard>:
{
    WinGet, activePath, ProcessPath, % "ahk_id" winActive("A")
    tool_pathandname = "%activePath%"
    run, %comspec% /c %tool_pathandname% -s -ro %A_ScriptDir%\custom\ae_scripts\01_Key-board.jsxbin ,,Hide
    return
}

<Ae_Script_QuickMenu>:
{
    WinGet, activePath, ProcessPath, % "ahk_id" winActive("A")
    tool_pathandname = "%activePath%"
    run, %comspec% /c %tool_pathandname% -s -ro %A_ScriptDir%\custom\ae_scripts\00_QuickMenu.jsxbin ,,Hide
    return
}
<Ae_Script_AEProject>:
{
    WinGet, activePath, ProcessPath, % "ahk_id" winActive("A")
    tool_pathandname = "%activePath%"
    run, %comspec% /c %tool_pathandname% -s -ro %A_ScriptDir%\custom\ae_scripts\AEProject.jsxbin ,,Hide
    return
}
<Ae_Script_batchFootage>:
{
    WinGet, activePath, ProcessPath, % "ahk_id" winActive("A")
    tool_pathandname = "%activePath%"
    run, %comspec% /c %tool_pathandname% -s -ro %A_ScriptDir%\custom\ae_scripts\commands\batchFootage.jsxbin ,,Hide
    return
}
<Ae_Script_CompSetter>:
{
    WinGet, activePath, ProcessPath, % "ahk_id" winActive("A")
    tool_pathandname = "%activePath%"
    run, %comspec% /c %tool_pathandname% -s -ro %A_ScriptDir%\custom\ae_scripts\commands\CompSetter.jsx ,,Hide
    return
}
<Ae_Script_AutoMatte>:
{
    WinGet, activePath, ProcessPath, % "ahk_id" winActive("A")
    tool_pathandname = "%activePath%"
    run, %comspec% /c %tool_pathandname% -s -ro %A_ScriptDir%\custom\ae_scripts\commands\cz_AutoMatte.jsxbin ,,Hide
    return
}
;调用动画预设文件
<Ae_Preset_Ani1>:
{
    WinGet, activePath, ProcessPath, % "ahk_id" winActive("A")
    tool_pathandname = "%activePath%"
    run, %comspec% /c %tool_pathandname% -ro %A_ScriptDir%\\custom\\ae_scripts\add_PresetAnimation_1.jsx ,,Hide
    return
}
<Ae_Preset_Ani2>:
{
    WinGet, activePath, ProcessPath, % "ahk_id" winActive("A")
    tool_pathandname = "%activePath%"
    run, %comspec% /c %tool_pathandname% -ro %A_ScriptDir%\\custom\\ae_scripts\add_PresetAnimation_2.jsx ,,Hide
    return
}
<Ae_Preset_Ani3>:
{
    WinGet, activePath, ProcessPath, % "ahk_id" winActive("A")
    tool_pathandname = "%activePath%"
    run, %comspec% /c %tool_pathandname% -ro %A_ScriptDir%\\custom\\ae_scripts\add_PresetAnimation_3.jsx ,,Hide
    return
}
<Ae_Preset_Ani4>:
{
    WinGet, activePath, ProcessPath, % "ahk_id" winActive("A")
    tool_pathandname = "%activePath%"
    run, %comspec% /c %tool_pathandname% -ro %A_ScriptDir%\\custom\\ae_scripts\add_PresetAnimation_4.jsx ,,Hide
    return
}
<Ae_Preset_Ani5>:
{
    WinGet, activePath, ProcessPath, % "ahk_id" winActive("A")
    tool_pathandname = "%activePath%"
    run, %comspec% /c %tool_pathandname% -ro %A_ScriptDir%\\custom\\ae_scripts\add_PresetAnimation_5.jsx ,,Hide
    return
}

<Ae_Help>:
run,https://blog.csdn.net/qq_14900371/article/details/84664743
return

<Ae_UpDater>:
Gui,Updating: +LastFound +AlwaysOnTop -Caption +ToolWindow
Gui,Updating: Color, %color2%
Gui,Updating: Font,cwhite s%FontSize% wbold q5,Segoe UI
Gui,Updating: Add, Text, ,%_AeUpdating%
Gui,Updating: Show,AutoSize Center NoActivate
UrlDownloadToFile, %UrlDownloadToFile_Ae1%, %A_ScriptDir%\plugins\AfterEffects\latest-Aftereffects.ahk ;

if ErrorLevel
{
    Gosub, AeExitUpdater
}
Else
{
    FileMove,%A_ScriptDir%\plugins\AfterEffects\latest-Aftereffects.ahk, %A_ScriptDir%\plugins\AfterEffects\Aftereffects.ahk,1
    Gosub, AeExitUpdater
}
return

AeExitUpdater:
FileDelete, %A_ScriptDir%\plugins\AfterEffects\latest-Aftereffects.ahk
sleep %SleepTime%
Gui,Updating: Hide
Return