;--AE VIMD 补充--------------------------------
;-------雙擊shift:关键帧曲线切换---------------------------------------
;-------雙擊Lctrl:K关键帧---------------------------------------
;-------按左鍵再按d:运行differenceToggle.jsx--------------------------------------
;-------按左鍵再按a:运行foolParent---------------------------------------
;-------按左鍵再按c
;-------按左鍵再按n:重命名图层
;-------按左鍵再按h:显示遮罩图形
SetTitleMatchMode, 2
#If WinActive("ahk_exe AfterFX.exe")
;----------------------------------------------

;----------------------------------------------
;快捷鍵區
;----------------------------------------------

;----------------------------------------------
;----------------------------------------------
;雙擊shift

~LShift::  
WinGet, activePath, ProcessPath, % "ahk_id" winActive("A")
tool_pathandname = "%activePath%"
if (A_PriorHotkey <> "~LShift" or A_TimeSincePriorHotkey > 400) 
{ 
	KeyWait, LShift
	return 
} 
;執行腳本
send +{F3}
return
;----------------------------------------------
;雙擊Lctrl

~LCtrl::  
WinGet, activePath, ProcessPath, % "ahk_id" winActive("A")
tool_pathandname = "%activePath%"
if (A_PriorHotkey <> "~LCtrl" or A_TimeSincePriorHotkey > 400) 
{ 
	KeyWait, LCtrl
	return 
} 
;執行腳本
run, %comspec% /c %tool_pathandname% -ro %A_ScriptDir%\\custom\\ae_scripts\add_keys.jsx ,,Hide
return
;----------------------------------------------
;按左鍵再按d

~LButton & d:: 
WinGet, activePath, ProcessPath, % "ahk_id" winActive("A")
tool_pathandname = "%activePath%"
KeyWait, LButton

;執行腳本
run, %comspec% /c %tool_pathandname% -ro %A_ScriptDir%\\custom\\ae_scripts\differenceToggle.jsx ,,Hide
return
;----------------------------------------------
;按左鍵再按a

~LButton & a:: 
WinGet, activePath, ProcessPath, % "ahk_id" winActive("A")
tool_pathandname = "%activePath%"
KeyWait, LButton

;if FileExist("%A_AppData%\\Ola script UI\cursorPos.txt")
CoordMode, Mouse,Screen
MouseGetPos, xpos, ypos  
FileDelete, %A_AppData%\\Ola script UI\cursorPos.txtcursorPos.txt
FileAppend, %xpos%`,%ypos%, %A_AppData%\\Ola script UI\cursorPos.txt

;執行腳本
run, %comspec% /c %tool_pathandname% -ro %A_ScriptDir%\\custom\\ae_scripts\foolParent.jsxbin ,,Hide
return
;----------------------------------------------
;按左鍵再按c

~LButton & c:: 
WinGet, activePath, ProcessPath, % "ahk_id" winActive("A")
tool_pathandname = "%activePath%"
KeyWait, LButton

send ^!{Home}

return
;----------------------------------------------
;按左鍵再按n

~LButton & n:: 
WinGet, activePath, ProcessPath, % "ahk_id" winActive("A")
tool_pathandname = "%activePath%"
KeyWait, LButton

CoordMode, Mouse,Screen
MouseGetPos, xpos, ypos  
FileDelete, %A_AppData%\\Ola script UI\cursorPos.txt
FileAppend, %xpos%`,%ypos%, %A_AppData%\\Ola script UI\cursorPos.txt

;執行腳本
run, %comspec% /c %tool_pathandname% -ro %A_ScriptDir%\\custom\\ae_scripts\simpleNamer 1.1.jsxbin ,,Hide

return
;----------------------------------------------
;按左鍵再按h

~LButton & h:: 
WinGet, activePath, ProcessPath, % "ahk_id" winActive("A")
tool_pathandname = "%activePath%"
KeyWait, LButton
;執行腳本
send +^H
send v
return
;----------------------------------------------
;按左鍵再按j

~LButton & j:: 
WinGet, activePath, ProcessPath, % "ahk_id" winActive("A")
tool_pathandname = "%activePath%"
KeyWait, LButton

;執行腳本
run, %comspec% /c %tool_pathandname% -ro %A_ScriptDir%\\custom\\ae_scripts\jumpToKey.jsx ,,Hide
return
;----------------------------------------------
;接到下面一層的outpoint
; !^[::
; send ^{down}
; send o
; send ^{up}
; send {PgDn}
; send ![
; return

;----------------------------------------------

;----------------------------------------------
#IfWinActive
