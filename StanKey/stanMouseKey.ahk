#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
#Warn  ; Recommended for catching common errors.
#SingleInstance, force ;Avoid multiple thread.
SetControlDelay 0
#UseHook
CoordMode, ToolTip, Screen

mouseToggle := false

LShift & LWin::
Critical, off
mouseToggle := not mouseToggle
ToolTip, %mouseToggle%
Sleep, 500
ToolTip
return

WheelRight::
Sleep, 200
send, {WheelRight}
return

WheelLeft::
Sleep, 200
send, {WheelLeft}
return

#If not WinExist("stanKey_") and mouseToggle == true
c::LButton
s::MButton
a::RButton

LButton::c
MButton::s
RButton::a

#If WinActive("stanMouseKey.ahk")
^s::
SendInput, ^s
Reload
return
