## 3Dsmax命令行研究 1

<p>命令行渲染根据可以让您不在打开max的情况渲执行批处理工作.</p>
<p>命令行渲染由3dsmaxcmd.exe 在Max目录中找到.</p>

#### 实例教程分析1

单个场景渲染：
1. 任意位置新建一个记事本文档，为了统一我们先取名为render1.bat。
2. 文档里输入以下内容（等会详细讲解）。


        @rem 设置程序路径
        set MaxPath="C:\Program Files\Autodesk\3ds Max 2014\3dsmaxcmd.exe"

        @rem 设置渲染对象
        set MaxObjPath="C:\Users\Administrator\Desktop\test\test.max"

        @rem 设置输出路径
        set MaxOutPath="C:\Users\Administrator\Desktop\test\output\test.png"

        %MaxPath% -outputname=%MaxOutPath% -w 800 -h 600 %MaxObjPath%

        pause

3. 保存，并把扩展名改为bat。

#### 下面讲解参数：

    1.set MaxPath="C:\Program Files\Autodesk\3ds Max 2014\3dsmaxcmd.exe"    
    设置程序路径，根据大家安装目录变化而变化。
    2.set MaxObjPath="C:\Users\Administrator\Desktop\test\test.max"         
    设置文件对象，你要渲染谁就找谁。
    3.set MaxOutPath="C:\Users\Administrator\Desktop\test\output\test.png"  
    设置渲染路径，格式可以直接填写
    4.%MaxPath% -outputname=%MaxOutPath% -w 800 -h 600 %MaxObjPath%         
    这句话的意思是调用3dsmaxcmd.exe， -outputname是输出名路径在MaxOutPath， -w -h是长宽属性 MaxObjPath文件【整体的格式是 程序+输出+设置+文件】
    5.pause 暂停


这样最简单的bat渲染就完成了，接下来我们稍微改装一下，把渲染配置放到单独的一个txt上


#### 实例教程分析2

1. 任意位置新建一个记事本文档，为了不跟上面冲突我们先取名为render2.bat
2. 任意位置新建一个记事本文档，为了统一我们先取名为rdPreset.txt

#### render2.bat文件输入以下内容

    @rem 设置程序路径

    set MaxPath="C:\Program Files\Autodesk\3ds Max 2014\3dsmaxcmd.exe"

    @rem 设置渲染对象

    set MaxObjPath="C:\Users\Administrator\Desktop\test\test.max"

    @rem 输出配置

    set rdPreset=@"C:\Users\Administrator\Desktop\test\rdPreset.txt"

    %MaxPath% %rdPreset% %MaxObjPath%

    pause

rdPreset文件输入以下内容
    -outputname=C:\Users\Administrator\Desktop\test\output\test2.png

    -width=800

    -height=600

这次的写法是把配置文件和bat文件分开，当然我们还可以单独一个文件专门来配置路径，bat调用参数call

接下来我们就说说一些配置参数
    -outputname 输出名

    -width      画布宽

    -height     画布高

    -camera     渲染哪个摄像机

    -start      开始帧

    -end        结束帧

    -frames     渲染范围 比如-frames=1-100 就是1到100帧 全部的话-frames=all 跳帧的话-frames=1,3,5-12
    
等等具体其它配置参数,请看
[官方帮助文档](https://knowledge.autodesk.com/zh-hans/support/3ds-max/learn-explore/caas/CloudHelp/cloudhelp/2017/CHS/3DSMax/files/GUID-E5239450-557C-4F51-8DBE-B9BE22F881CA-htm.html)，打开网页往下拉即可查阅.
