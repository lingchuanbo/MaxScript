## 3Dsmax命令行研究 2

<p>这次讲解下多个max渲染配置.</p>


#### 实例教程分析3

1. 任意位置新建一个记事本文档，为了不跟上面冲突我们先取名为render2.bat
2. 任意位置新建一个记事本文档，为了统一我们先取名为rdPreset.txt
3. 规范进行到底我们要新建SetEnv.bat/getMax.bat/outPath.bat

        setEnv:设置Max路径
        getMax:设置获取max文件
        outPath:设置渲染目录

    这样利于项目开发便于管理，批渲染脚本易读比较高,按照这个框架目前适合项目批处理开发，是我自己想的过，你爱信不信！

#### render.bat文件输入以下内容

        call SetEnv.bat   	--调用文件取值max
        call getMax.bat		--调用文件渲染max对象
        call outPut.bat		--调用文件取值输出路径

        @rem 渲染配置 可以设置多个预设但要注意名字
        set rdPreset=@"C:\Users\Administrator\Desktop\test\rdPreset.txt" 

        echo. 开始渲染%getMaxName%

        @rem 一行代表渲染一个对象，多个要渲染多个，注意定义取值，getMax.bat和outPut.bat
        %Max% %rdPreset% -o=%outPathName1% %getMaxName1% 

        echo. 渲染%getMaxName%完成

        echo. 开始渲染%getMaxName1%

        %Max% %rdPreset% -o=%outPathName2% %getMaxName2% 

        echo. 渲染%getMaxName1%完成
        pause



rdPreset文件输入以下内容:

    -width=800

    -height=600

    -frames=1,3,5-6

SetEnv.bat文件输入以下内容:

        @rem 设置程序路径
        Set Max="C:\Program Files\Autodesk\3ds Max 2014\3dsmaxcmd.exe"

getMax.bat文件输入以下内容:

        @rem 设置渲染对象
        Set getMaxName1="C:\Users\Administrator\Desktop\test\test1.max"
        Set getMaxName2="C:\Users\Administrator\Desktop\test\test2.max"

outPut.bat文件输入以下内容:

        @rem 输出配置
        Set outPathName1="C:\Users\Administrator\Desktop\test\output\test1.png"
        Set outPathName2="C:\Users\Administrator\Desktop\test\output\test2.png"


<p>
入门差不多这样，接下来下来我主要来测试渲染配置参数的应用
</p>
<p>
瞎折腾...
</p>
<p>
ps:有问题的一般都是路径配置问题，所以在配置过程中要注意在注意！
</p>

    源文件:3Dsmax命令行研究 2_test.7z 自行下载

<p>
批处理还有一些实用的命令比如，渲染完关机,加在末尾就可以！
</p>
        shutdown -s -t 100 -c "渲染完啦，尼玛！！！主人跑了关机 ... ..."

        -s是关机，-t 100是读秒时间单位秒，这里是100秒
<p>
希望大家玩得66666
</p>
