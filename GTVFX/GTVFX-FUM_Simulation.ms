/*
/* FumeFX QuickSim v1.0
Created:      July  04, 2014
Author :  HOOLONG (hoolongvfx@gmail.com)

*/
/*
macroScript FFX_LightArray
	category:"**HOOLONG_VFX**"
	buttonText:"LightArray "
	tooltip:"FFX_LightArray v1.0"
	icon:#("Lights",3)
	(
*/
--try(destroydialog FumeFXSimulation)catch()

if FumeFXSimulation != undefined do
	if classOf FumeFXSimulation == RolloutClass do
		destroyDialog FumeFXSimulation
rollout FumeFXSimulation "FumeFXSimulation " width:200 height:150
(
	button btn1 "Very Low" pos:[10,29] width:80 height:21
	button btn2 "Low" pos:[104,29] width:80 height:21
	button btn3 "Medium" pos:[10,79] width:80 height:21
	button btn4 "High" pos:[104,79] width:80 height:21


	fn radioBtn s eb =
	(
		if s.modifierKeys==s.modifierKeys.Control then
		(
			if matchpattern s.text pattern:"*  +" == false do (s.text += "  +"; s.tag += "  +")
			mui.qualityArray[s.name as number] = mysim.SolverQuality
			mui.iterArray[s.name as number] = mysim.MaximumIterations
		)
		else
		(
			ArrayNumber = s.name as number
			x = mysim.width/100
			y = mysim.length/100
			z = mysim.height/100
			voxelCount = mui.radioBtnVoxelCountArray[ArrayNumber]*mui.Multi
			calcSpacing = (formattedPrint(pow (((x*y*z)/voxelCount)) ((1.0/3)))format:"1.2f") as number
			if mui.multiSimContainer.count == 0 then
			(
				mysim.GridSpacing = calcSpacing
				mysim.SolverQuality = mui.qualityArray[ArrayNumber]
				mysim.MaximumIterations = mui.iterArray[ArrayNumber]
			)
			else
			(
				for i = 1 to mui.multiSimContainer.count do
				(
					mysim = mui.multiSimContainer[i]
					mysim.GridSpacing = calcSpacing
					mysim.SolverQuality = mui.qualityArray[ArrayNumber]
					mysim.MaximumIterations = mui.iterArray[ArrayNumber]
				)
			)
		)
	)
)
createdialog  FumeFXSimulation 



